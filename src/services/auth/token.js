import IdTokenVerifier from 'idtoken-verifier'

const store = window.localStorage

export const BEARER_STRING = 'Bearer '

/**
 * Verify the token integrity
 *
 * @param {String} token
 */
function verify (token) {
  const check = (new IdTokenVerifier()).decode(token)
  return Boolean(check?.header?.alg === 'HS256')
}

/**
 * Get token key from storage
 *
 * @param {String} key
 */
export function getToken (key) {
  const token = store.getItem(key)

  if (!token) {
    removeToken(key)
    return undefined
  }

  const isValid = verify(token)

  if (isValid) {
    return token
  }

  removeToken(key)
  return undefined
}

/**
 * Remove token by key
 *
 * @param {String} key
 */
export function removeToken (key) {
  store.removeItem(key)
}

/**
 * Set token on localStorage
 *
 * @param {String} key
 * @param {String} value
 */
export function setToken (key, value) {
  key && value && store.setItem(key, value)
}
