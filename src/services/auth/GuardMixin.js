import { getToken } from './token'

export default function GuadMixin ({
  authKey,
  locationPath,
  loginPath,
  ignore
}) {
  return {
    name: 'GuardMixin',

    methods: {

      /**
       * Check if user needs to be redirected on the ref
       * path or custom
       */
      checkRedirect () {
        const path = window.location.pathname

        if (path !== loginPath) {
          this.$router.replace({
            path: loginPath,
            query: {
              ref: this.redirectPath(path || locationPath)
            }
          })
        }
      },

      /**
         * Resolve redirect path, exclude routes
         *
         * @param {String} path
         * @return {String}
         */
      redirectPath (path) {
        const match = this.$router.resolve(path)
        const route = match?.route

        return (
          route &&
          !ignore.includes(route.name) &&
          route.path
        ) || undefined
      },

      /**
       * Redirect from login if user is already logged in
       */
      protectLogin () {
        if (window.location.pathname === loginPath) {
          this.$router.push({
            path: locationPath
          })
        }
      }
    },

    created () {
      !getToken(authKey)
        ? this.checkRedirect()
        : this.protectLogin()
    }
  }
}
