import axios from 'axios'
import defaultTransform from './defaultTransform'
import { apiRoot } from 'utils//env'

function bearerAuth (token) {
  return `Bearer ${token}`
}

export function setGlobalToken (token) {
  def.headers.Authorization = bearerAuth(token)
}

const def = {
  baseURL: apiRoot,
  headers: {}
}

/**
 * Base axios config wrapper around resolving
 * base url forproduction and development
 */
export function apiConfig (userConfig = {}) {
  const config = {
    transformResponse: [
      defaultTransform
    ],
    ...def,
    ...userConfig
  }

  return axios.create(config)
}

/**
 *
 * @param {String} url
 * @param {Object} opts
 */
export function get (url, opts = {}) {
  return apiConfig().get(url, opts)
}

/**
 *
 * @param {String} url
 * @param {Object} data
 * @param {Object} opts
 */
export function post (url, data, opts = {}) {
  return apiConfig().post(url, data, opts)
}

/**
 *
 * @param {String} url
 * @param {Object} data
 * @param {Object} opts
 */
export function put (url, data, opts = {}) {
  return apiConfig().put(url, data, opts)
}

/**
 *
 * @param {String} url
 * @param {Object} data
 * @param {Object} opts
 */
export function del (url, data, opts = {}) {
  return apiConfig().delete(url, data, opts)
}
