import { get } from '.'

export function fetchUserProfile () {
  try {
    const response = get('profile')
    return response?.data
  } catch (e) {
    const err = e?.response?.data?.message || 'USER_FETCH_ERROR'
    throw err
  }
}
