// If we have errors
// Just return them, and catchers will throw
export default function defaultTransform (req) {
  const data = JSON.parse(req || {})
  return (data && data?.data) || data?.message
}
