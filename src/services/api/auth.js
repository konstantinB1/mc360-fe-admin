import { post } from '.'

export async function loginWithData (data) {
  try {
    const response = await post('/authenticate/login', data)
    return response
  } catch (e) {
    const err = e?.response?.data?.message || 'AUTH_ERROR'
    throw err
  }
}
