class StyleVarManager {
  constructor (id, props) {
    this._id = id
    this._style = null
    this._vars = {}
    this._global = process.env._STYLES_

    if (document.getElementById(this._id)) {
      console.warn(`
        VueTheme is already instanciated,
        or uses the same style id
      `)

      return
    }

    this.create()

    if (props) {
      this.addProperties(props)
    }
  }

  create () {
    this._style = document.createElement('style')
    this._style.type = 'text/css'
    this._style.setAttribute('id', this._id)
    document.head.appendChild(this._style)
  }

  remove () {
    document.head.removeChild(this._style)
  }

  addProperties (props) {
    this._vars = {
      ...this._vars,
      ...props
    }

    this._inject()
  }

  _inject () {
    const vars = this._vars

    if (!Object.keys(vars).length) {
      return
    }

    let root = ':root { \n'

    for (const key in vars) {
      const value = vars[key]
      root += `--${key}:${value}; \n`
    }

    root += ' }'

    this._style.textContent = root
  }
}

export default StyleVarManager
