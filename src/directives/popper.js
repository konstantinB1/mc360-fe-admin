import { createPopper } from '@popperjs/core'

export default {
  bind (el, { value = {} }, { $el }) {
    if (!el) {
      return
    }

    const {
      ref = $el,
      strategy = 'fixed',
      placement = 'auto',
      popperModifiers = []
    } = value

    if (ref) {
      createPopper(ref, el, {
        placement,
        strategy,
        modifiers: popperModifiers
      })
    }
  }
}
