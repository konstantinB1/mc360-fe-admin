import { VLOADER_SET_PROPS } from './types'
import { initialProps } from './state'

export default {
  [VLOADER_SET_PROPS]: (state, data) => {
    state.props = { ...data }

    if (data.destroy) {
      state.props = initialProps
    }
  }
}
