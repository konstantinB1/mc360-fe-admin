export const initialProps = {
  loading: false,
  text: null,
  icon: null,
  title: null
}

export default {
  state: initialProps
}
