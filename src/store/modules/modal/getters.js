import { GET_MODAL_BY_UUID } from './types'

export default {
  [GET_MODAL_BY_UUID]: state => uuid => (
    state.find(o => o.uuid === uuid)
  )
}
