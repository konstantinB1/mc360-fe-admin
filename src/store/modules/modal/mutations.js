import {
  ADD_MODAL_TO_LIST,
  RM_MODAL_FROM_LIST
} from './types'

export default {
  [ADD_MODAL_TO_LIST] (state, payload) {
    state.list.push(payload)
  },

  [RM_MODAL_FROM_LIST] (state, uuid) {
    state.list = state.list.filter(o => o.uuid !== uuid)
  }
}
