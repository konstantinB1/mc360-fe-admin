import {
  ADD_MODAL_TO_LIST,
  RM_MODAL_FROM_LIST,
  MODAL_MOUNT,
  MODAL_UNMOUNT
} from './types'
import uuid from 'utils/uuid'

import * as TYPES from './modalTypes'

function getModalType (type) {
  const {
    MODAL_DELETE_PROMPT,
    MODAL_PROCESSING
  } = TYPES

  switch (type) {
    case MODAL_DELETE_PROMPT:
      return import('components/layout/Modal/ModalDeleteConfirm')
    case MODAL_PROCESSING:
      return import('components/layout/Modal/ModalProcessing')
  }
}

export default {
  async [MODAL_MOUNT] ({ commit }, payload) {
    try {
      const backdropProps = payload?.backdropProps || {}
      const component = await getModalType(payload?.type)
      const uuid$1 = uuid()
      const data = {
        ...payload,
        backdropProps,
        component: component?.default,
        uuid: uuid$1
      }

      commit(ADD_MODAL_TO_LIST, data)

      return data
    } catch (e) {
      throw new Error(`Unable to resolve modal component: ${payload?.type}`)
    }
  },

  [MODAL_UNMOUNT] ({ commit }, payload) {
    console.log(payload)
    commit(RM_MODAL_FROM_LIST, payload)
  }
}
