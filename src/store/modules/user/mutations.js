import { SET_USER_DATA } from './types'

export default {
  [SET_USER_DATA] (state, payload) {
    state.data = payload
  }
}
