export default {
  isError: null,
  isInitAuth: false,
  isFinishedAuth: false,
  isSuccess: false,
  token: null
}
