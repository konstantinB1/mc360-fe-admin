import {
  IS_AUTH_INIT,
  IS_AUTH_ERROR,
  IS_AUTH_FINISH,
  IS_AUTH_SUCCESS
} from './types'

import { setToken } from 'services/auth/token'
import { TOKEN_KEY } from 'config'

export default {
  [IS_AUTH_INIT] (state) {
    state.isInitAuth = true
  },

  [IS_AUTH_FINISH] (state) {
    state.isInitAuth = false
    state.isFinishedAuth = true
  },

  [IS_AUTH_ERROR] (state, payload) {
    state.isSuccess = false
    state.isFinishedAuth = true
    state.isError = payload
  },

  [IS_AUTH_SUCCESS] (state, payload) {
    state.isSuccess = true
    state.isError = null

    setToken(TOKEN_KEY, payload)
  }
}
