import {
  AUTH_WITH_DATA,
  IS_AUTH_INIT,
  IS_AUTH_FINISH,
  IS_AUTH_SUCCESS,
  IS_AUTH_ERROR
} from './types'
import {
  USER_NAMESPACE,
  SET_USER_DATA
} from '../user/types'

import { setGlobalToken } from 'services/api'
import { loginWithData } from 'services/api/auth'
import { setToken } from 'services/auth/token'
import { TOKEN_KEY } from 'config'

export default {

  /**
   * Init auth with login data params
   *
   * @param {Object} { commit }
   * @param {Object} payload
   */
  async [AUTH_WITH_DATA] ({ commit }, payload) {
    commit(IS_AUTH_INIT)
    const params = {
      email: payload?.email,
      password: payload?.password
    }

    try {
      const response = await loginWithData(params)
      const {
        token,
        ...data
      } = response?.data

      if (data && token) {
        commit(IS_AUTH_SUCCESS)
        setToken(TOKEN_KEY, token)
        setGlobalToken(TOKEN_KEY, token)

        commit(`${USER_NAMESPACE}/${SET_USER_DATA}`, data, {
          root: true
        })

        return data
      }
    } catch (e) {
      commit(IS_AUTH_ERROR, e)
      throw e
    } finally {
      commit(IS_AUTH_FINISH)
    }
  }
}
