export default {
  dashboard: 'Dashboard',
  customers: 'Customers',
  customers_add: 'Add customer',
  customers_view: 'View customer',
  customers_add_desc: 'Add new customer',
  customers_edit_desc: 'Edit customer - %{customer}',
  shipments: 'Shipment | Shipments',
  pickup: 'Pickup',
  booking: 'Booking | Bookings'
}
