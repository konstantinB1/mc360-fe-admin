export default {
  actions: 'Actions',
  add: 'Add',
  add_customer: 'Add Customer',
  apply: 'Apply',
  cancel: 'Cancel',
  confirm: 'Confrim',
  confirm_delete: 'Yes, delete it',
  clear_all_filters: 'Clear all filters',
  delete: 'Delete',
  delete_selected: 'Delete selected',
  delete_confirm_heading: 'Confirm delete',
  delete_confirm_subtext: 'Are you sure you want to delete this data?',
  delete_confirm_button: 'DELETE',
  dt_filters_rm_pill_desc: 'Remove filter',
  modify: 'Modify',
  hide: 'Hide'
}
