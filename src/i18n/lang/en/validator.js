export default {
  must_not_be_empty: 'This field is required',
  invalid_email_format: 'Invalid email format'
}
