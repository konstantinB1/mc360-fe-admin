export default {
  inputPlaceholder: 'Insert manually (format {format})',
  daysShort: {
    mon: 'Mo',
    tue: 'Tu',
    wen: 'We',
    thu: 'Th',
    fri: 'Fr',
    sat: 'Sa',
    sun: 'Su'
  },
  daysLong: ''
}