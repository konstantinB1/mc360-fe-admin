import Vue from 'vue'
import VueI18n from 'vue-i18n'
import messages from './lang/en'
import axios from 'axios'

Vue.use(VueI18n)

export const i18n = new VueI18n({
  locale: 'en',
  fallbackLocale: 'en',
  silentTranslationWarn: true,
  messages: { en: messages }
})

const loadedLanguages = ['en']

function setI18nLanguage (lang) {
  i18n.locale = lang
  axios.defaults.headers.common['Accept-Language'] = lang
  document.querySelector('html').setAttribute('lang', lang)
  return lang
}

// export function loadLangFragments (fragList) {
//   const createList = fragList.map
// }

export async function loadLanguageAsync(lang) {
  if (i18n.locale === lang || loadedLanguages.includes(lang)) {
    return await setI18nLanguage(lang)
  }

  const curLang = await import(`./lang/${lang}`)

  i18n.setLocaleMessage(lang, curLang.default)
  loadedLanguages.push(lang)
  setI18nLanguage(lang)
}