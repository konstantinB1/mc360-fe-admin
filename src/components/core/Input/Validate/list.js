import isEmpty, { messages as isEmptyMessages } from './types/isEmpty'
import minMax, { messages as minMaxMessages } from './types/minMax'
import email, { messages as emailMessages } from './types/email'

export default {
  email: {
    handler: email,
    messages: emailMessages
  },
  isEmpty: {
    handler: isEmpty,
    messages: isEmptyMessages
  },
  minMax: {
    handler: minMax,
    messages: minMaxMessages
  }
}
