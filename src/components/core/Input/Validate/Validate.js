import { forOwn, isPlainObject, isFunction } from 'lodash'
import builtIn from './list'

export default class Validate {
  constructor ({ data, scheema }) {
    this.scheema = scheema
    this.data = data
    this._isValid = null
    this._errors = []

    this._check()
  }

  _setError (key, messages) {
    const errorsIndex = this._errors.findIndex(o => o.name === key)
    errorsIndex === -1
      ? this._errors.push({
        name: key,
        messages
      }) : (() => {
        const { messages: newErrors } = this._errors[errorsIndex]
        this._errors.splice(errorsIndex, 1, {
          name: key,
          messages: [
            ...newErrors,
            ...messages
          ]
        })
      })()
  }

  _check () {
    const { data, scheema } = this

    forOwn(scheema, (value, key) => {
      const inputValue = data[key]

      value.forEach((type) => {
        if (isPlainObject(type)) {
          const { name, params } = type

          if (name === 'custom' && isFunction(params)) {
            const messages = params(inputValue)
            messages && this._setError(key, messages)
          } else {
            const { handler, messages } = builtIn[name]
            handler(inputValue, params) && this._setError(key, messages)
          }
        } else if (typeof type === 'string') {
          const { handler, messages } = builtIn[type]
          handler(inputValue) && this._setError(key, messages)
        }
      })
    })
  }

  clear () {
    this._errors = []
  }

  get errors () {
    return this._errors
  }

  get isValid () {
    return !this._errors.length
  }
}
