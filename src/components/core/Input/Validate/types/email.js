import _normalize from './_normalize'

export const messages = ['validator.invalid_email_format']

export default function email (v) {
  return !(/\S+@\S+\.\S+/.test(_normalize(v)))
}
