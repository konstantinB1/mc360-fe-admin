import _normalize from './_normalize'

export const messages = ['validator.must_not_be_empty']

export default function isEmpty (v) {
  return String(_normalize(v)).length === 0
}
