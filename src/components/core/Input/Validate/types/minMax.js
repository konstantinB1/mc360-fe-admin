export const messages = [
  'validator.must_be_min',
  'validator.must_be_max'
]

export default function minxMax (v, [min, max]) {
  const s = String(v)
  const len = s.length

  return len < min || len > max
}
