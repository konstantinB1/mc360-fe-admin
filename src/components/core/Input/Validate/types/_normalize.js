export default function _normalize (v) {
  return (v === void 0 || v === null || v === 'undefined') ? '' : v
}
