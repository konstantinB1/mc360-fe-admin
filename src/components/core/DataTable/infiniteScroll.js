export default {
  abstract: true,

  methods: {
    scrollRoller () {
      const { offset } = this.infiniteScroll
      const { bottom } = this.scrollEl.getBoundingClientRect()
      const scanCurrentBound = +(Math.round(bottom - window.innerHeight))

      if (scanCurrentBound <= offset) {
        this.removeListener()
        this.fetchFn(true)

        const unwatch = this.$watch('loader', loading => {
          if (!loading) {
            this.addListener()
            unwatch()
          }
        })
      }
    },

    removeListener () {
      this.infiniteScroll.container.removeEventListener('scroll', this.scrollRoller, true)
    },

    addListener () {
      this.infiniteScroll.container.addEventListener('scroll', this.scrollRoller, true)
    }
  },

  mounted () {
    this.$nextTick(() => {
      const { container } = this.infiniteScroll
      this.scrollEl = container === document ? document.documentElement : container
    })
  },

  watch: {
    'rows.length' (len) {
      const { loadThreshold } = this.infiniteScroll

      if (len < loadThreshold) {
        return
      }

      this.addListener()
    }
  }
}
