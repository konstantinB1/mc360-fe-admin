import { parse } from 'qs'
import { transform } from 'lodash'

const defaults = {
  search: {},
  order: 'id',
  type: 'desc'
}

export default {
  data: () => ({
    queryObject: null,
    init: true
  }),

  computed: {
    normConfig () {
      return this.dataConfig.flat()
    },

    localQuery () {
      return this.queryObject?.[this.tableName] || {}
    },

    localRouteQuery () {
      const { tableName } = this
      const { query } = this.$route
      return parse(query?.[tableName] || '')
    }
  },

  methods: {
    resolveBrowserQuery (query) {
      return {
        ...parse(location.search.slice(1)),
        [this.tableName]: query
      }
    },

    resolveValue (value, name) {
      const { tableName, localQuery } = this
      this.queryObject = {
        [tableName]: {
          ...defaults,
          ...localQuery,
          search: {
            ...defaults.search,
            ...localQuery?.search,
            [name]: value
          }
        }
      }
    },

    removeValueFromSearch (localName) {
      const { localQuery, applyHandler } = this

      const remove = localName === '_all'
        ? {}
        : transform(localQuery.search, (data, value, key) => {
          (key !== localName) && (data[key] = value)
        })

      this.queryObject = this.resolveBrowserQuery({
        ...localQuery,
        search: remove
      })

      applyHandler()
    }
  },

  created () {
    const {
      localRouteQuery,
      resolveBrowserQuery,
      applyHandler,
      normConfig
    } = this

    this.queryObject = resolveBrowserQuery(localRouteQuery)
    this.listWithValues = transform(normConfig, (data, { name, ...value }) => {
      data.push({
        ...value,
        name,
        value: localRouteQuery?.search?.[name]
      })
    }, [])

    applyHandler()
  }
}
