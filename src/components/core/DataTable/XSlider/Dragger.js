import uuid from 'utils/uuid'

class Dragger {
  constructor (el, id, ctx) {
    this.dragging = false
    this.self = this
    this.el = el
    this.id = id
    this.ctx = ctx
    this.coords = {
      start: null,
      end: null
    }
  }

  setEvents () {
    const {
      el,
      mouseOut,
      mouseClick,
      dragEvent,
      moveEvent
    } = this

    el.addEventListener('mouseup', mouseOut)
    el.addEventListener('mousedown', mouseClick, true)
    el.addEventListener('mousemove', moveEvent)
    el.addEventListener('drag', dragEvent)
  }

  mouseOut (_e) {
    this.draging = false
  }

  mouseClick ({ offsetX }) {
    const { coords } = this
    this.dragging = true

    if (offsetX > 0) {
      coords.start = offsetX
    }
  }

  moveEvent ({ offsetX }) {
    const { coords, ctx, dragging } = this

    if (dragging) {
      const pos = -(coords.start - offsetX)

      ctx.$set(ctx, 'a', pos)
    }
  }

  dragEvent (e) {

  }
}

let dragger

export default {
  bind (nodeEl, binds, vNode) {
    const { totalRowWidth, config } = binds
    const { context } = vNode
    const id = nodeEl.getAttribute('id') || uuid()

    dragger = new Dragger(nodeEl, id, context)
  }
}
