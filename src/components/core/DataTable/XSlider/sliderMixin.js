export default {
  props: {
    rowWidth: Number,
    items: Array,
    containerRef: HTMLElement
  },

  data: () => ({
    containerWidth: null,
    slideXPos: 0
  }),

  computed: {
    itemsWidth () {
      const { isArray } = Array
      return this.items.map(item => isArray(item) ? item[0].width : item.width)
    },

    spaceRemaining () {
      return this.rowWidth - this.containerWidth - Math.abs(this.slideXPos)
    },

    neraestOffset () {
      const { itemsWidth, slideXPos } = this
      const pos = Math.abs(slideXPos)

      return itemsWidth.some(w => {
        if (w < pos) {
          return pos
        }
      })
    }
  },

  methods: {
    slideLeft () {
      this.slideXPos = 0

      this.$emit('position', this.slideXPos)
    },

    slideRight () {
      const { slideXPos, neraestOffset, spaceRemaining } = this
      const { abs } = Math
      const slideMax = spaceRemaining < MAX_PX ? spaceRemaining : MAX_PX

      this.slideXPos = -abs(slideXPos - slideMax)
      this.$emit('position', this.slideXPos)
      console.log(neraestOffset)
    }
  },

  mounted () {
    this.containerWidth = this.$parent.$el.getBoundingClientRect().width
  }
}
