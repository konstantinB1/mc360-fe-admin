export default {
  format: {
    type: String,
    required: false,
    default: 'dd.MM.yyyy'
  },
  range: {
    type: Boolean,
    required: false
  },
  value: {
    type: [String, Date],
    required: false,
    default: () => new Date()
  }
}
