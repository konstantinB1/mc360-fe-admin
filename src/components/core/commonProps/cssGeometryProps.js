export default {
  alignItems: String,
  display: String,
  flexDirection: String,
  flexWrap: String,
  justifyContent: String,
  position: String
}
