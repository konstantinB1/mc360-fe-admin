import { colorProp } from './colors'

export default {
  color: colorProp,
  dimensions: {
    type: [Number, Array],
    required: false
  },

  type: {
    type: String,
    required: false
  },

  watch: {
    type: Boolean,
    required: false,
    default: false
  },

  size: {
    type: Number,
    required: false,
    default: 26
  }
}
