import colorList from '../styles/colors.scss'

export const colorProp = {
  type: String,
  required: false,
  default: 'dark',
  validator: v => Object.keys(colorList).includes(v)
}
