export default {
  filters: {
    type: Array,
    required: false
  },
  iconComponent: {
    type: Object,
    required: false
  },
  items: {
    type: [Array, Object],
    required: false
  },
  multi: {
    type: Boolean,
    required: false
  },
  search: {
    type: Boolean,
    required: false
  },
  value: {
    type: [String, Number, Array],
    reqired: true
  }
}
