import { getMonthDays, getEnumDays, getDayStrName } from './utils'

export default {
  abstract: true,

  name: 'Datepicker',

  render () {
    return this.$slots.default
  },

  props: {
    date: {
      type: Date,
      required: false,
      default: () => new Date()
    },

    format: {
      type: Boolean,
      required: false,
      default: true
    },

    setDate: {
      type: [Array, String, Function],
      required: false
    }
  },

  data: () => ({
    currentMonthPanel: []
  }),

  computed: {
    getMonth () {
      return this.date.getMonth()
    },

    getFullYear () {
      return this.date.getFullYear()
    },

    formatMatrixGrid () {
      const format = [[], [], [], [], [], []]
      const len = this.currentMonthPanel.length

      for (let i = 0, r = 0; i < len; i++) {
        const row = format[r]
        const rLen = row.length
        const item = this.currentMonthPanel[i]

        if (rLen === 6) {
          row.push(item)
          r++
          continue
        }

        row.push(item)
      }

      return format
    }
  },
  methods: {
    setDateItem (m, d) {
      return new Date(Date.UTC(this.getFullYear, m, d))
    },

    computePanel (date = this.date) {
      this.currentMonthPanel = []

      const month = this.date.getMonth()
      const year = this.date.getFullYear()

      const getUTCFirstInMonth = new Date(Date.UTC(year, month, 1))
      const monthsDays = getMonthDays(this.date)
      const getDayStringName = getDayStrName(getUTCFirstInMonth)

      if (getDayStringName !== 'mon') {
        const minDays = getEnumDays(getDayStringName)
        const getDaysInMonthBefore = this.getMonth === 0 ? monthsDays[11] : monthsDays[month - 1]
        const getLastMonthIndex = this.getMonth === 0 ? 11 : this.getMonth - 1
        const len = getDaysInMonthBefore - minDays

        for (let i = getDaysInMonthBefore; i > len; i--) {
          this.currentMonthPanel.unshift(this.setDateItem(getLastMonthIndex, i))
        }
      }

      const computeCurMonth = month >= 11 ? 0 : month
      const curMonth = monthsDays[computeCurMonth]

      for (let i = 1; i <= curMonth; i++) {
        this.currentMonthPanel.push(this.setDateItem(month, i))
      }

      const curPanelLen = this.currentMonthPanel.length
      const DATE_MATRIX_CALC = 42

      const getRemaingingDiff = DATE_MATRIX_CALC - curPanelLen

      for (let i = 1; i <= getRemaingingDiff; i++) {
        this.currentMonthPanel.push(this.setDateItem(month + 1, i))
      }

      if (this.format)
        this.$emit('panel', this.formatMatrixGrid)
      else
        this.$emit('panel', this.currentMonthPanel)
    }
  },

  watch: {
    date: {
      deep: true,
      immediate: true,
      handler (date) {
        this.computePanel(date)
      }
    }
  }
}
