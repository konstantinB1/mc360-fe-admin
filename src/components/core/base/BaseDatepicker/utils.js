
/**
   * @param  {Date} date
   * @return {Number|Array}
   */
export function getMonthDays (date, month) {
  const year = date.getFullYear()
  const months = [31, year % 4 === 0 ? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
  if (!month)
    return months

  return months[month]
}

/**
   * @param {Number} day
   * @return {Number|Object}
   */
export function getEnumDays (day) {
  const days = {
    'mon': 0,
    'tue': 1,
    'wed': 2,
    'thu': 3,
    'fri': 4,
    'sat': 5,
    'sun': 6
  }

  if (!day)
    return days

  return days[day]
}

/**
 * @param  {Date}
 * @return {String}
 */
export function getDayStrName (date) {
  return date.toDateString().slice(0, 3).toLowerCase()
}
