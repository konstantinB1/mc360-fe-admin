export default {
  props: {
    iconComponent: {
      type: Object,
      required: false
    },
    items: {
      type: [Array, Object],
      required: false
    },
    multi: {
      type: Boolean,
      required: false
    },
    search: {
      type: Boolean,
      required: false
    },
    value: {
      type: [String, Number, Array],
      reqired: true
    }
  }
}
