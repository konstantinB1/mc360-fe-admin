import countryList from 'data/country'
import { transform } from 'lodash'

export default {
  props: {
    filter: [Array, String],
    countryKey: {
      type: String,
      default: 'alpha2'
    }
  },

  computed: {
    countryItems () {
      const { countryKey } = this

      const list = transform(countryList, (res, value) => {
        const { name, alpha2 } = value

        res[value[countryKey]] = {
          value: name,
          icon: alpha2.toLowerCase()
        }
      }, {})

      return list
    }
  }
}
