import { colorProp } from '../commonProps/colors'

export default {
  color: colorProp,
  iconProps: {
    type: [String, Object],
    required: false,
    default: null
  },

  typographyProps: {
    type: Object,
    required: false,
    default: () => ({
      tag: 'span',
      size: 13,
      weight: 600,
      color: 'white'
    })
  },

  type: {
    type: String,
    default: 'main'
  }
}
