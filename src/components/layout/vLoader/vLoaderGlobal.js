import vLoader from './vLoader'
import { createNamespacedHelpers } from 'vuex'

const { mapState } = createNamespacedHelpers('vloader')

export default {
  render (h) {
    if (this.vloaderProps.loading) {
      return h(vLoader, { props: this.vloaderProps })
    }
  },

  components: {
    vLoader
  },

  computed: mapState({
    vloaderProps: ({ props }) => {
      return props
    }
  })
}
