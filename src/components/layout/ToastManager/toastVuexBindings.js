import { isFunction } from 'lodash'

export const TOAST_ADD = 'TOAST_ADD'
export const TOAST_RM = 'TOAST_RM'
export const TOAST_MODIFY = 'TOAST_MODIFY'

export const state = {
  list: []
}

export const mutations = {
  [TOAST_ADD] (state, opts) {
    state.list.push(opts)
  },

  [TOAST_RM] (state, idOrFn) {
    state.list = state.filter(
      (isFunction(idOrFn) && idOrFn) ||
      (({ id }) => id === idOrFn)
    )
  },

  [TOAST_MODIFY] (state, data, id) {
    const index = state.list.findIndex(o => o.id === id)

    if (index) {
      state.list.splice(index, 1, data)
    }
  }
}

export const computed = {
  getById: state => id => state.list.find(o => id => o.id)
}

export const actions = {
  
}