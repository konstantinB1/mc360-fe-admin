export default {
  props: {
    appear: Boolean,
    css: Boolean,
    group: Boolean,
    mode: String,
    name: String,
    duration: Number
  }
}
