
export default {
  'main-background': 'rgba(248, 249, 250)',
  'color-main-low-opacity': 'rgba(76, 157, 157, 0.2)',
  'color-main': 'rgba(76, 157, 157)',
  'color-dark': 'rgba(54, 61, 68, 1.0)',
  'color-light': 'rgba(152, 159, 166)',
  'color-gray-light': 'rgba(209, 213, 216)',
  'color-warning': '#e86c60',
  'color-header-border': 'rgba(223, 223, 223)',
  'color-header-bg': 'rgba(255, 255, 255)',
  'color-save': '#72C472',
  'color-btn-cancel': '#D1D5D8',
  'header-search-border': 'rgba(223, 223, 223)',
  'main-border-radius': '6px',
  'header-online-status': 'rgba(114, 196, 114)',
  'header-away-status': 'rgba(239, 211, 88)',
  'header-busy-status': 'rgba(232, 108, 96)',
  'main-shadow': '0px 0px 10px 2px rgba(232, 235, 237, 0.9)',
  'main-button-color': 'rgba(209, 213, 216)'
}
