export default {
  name: 'DEV_TEST',
  path: '/admin/_DEV_TEST_',
  component: () => import(
    /* webpackChunkName: "route.moduleIndex" */ './DevTest'
  )
}
