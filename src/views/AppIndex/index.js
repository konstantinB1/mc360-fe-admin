export default {
  path: '/admin',
  name: 'index',
  component: () => import(
    /* webpackChunkName: "route.moduleIndex" */ './AppIndex'
  )
}
