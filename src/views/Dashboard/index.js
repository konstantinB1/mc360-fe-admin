import CustomersIndex from './Customers/CustomersIndex'
import CustomersView from './Customers/CustomersView'
import CustomersActions from './Customers/CustomersActions'

export default {
  path: '/admin/dashboard',
  name: 'routeNames.dashboard',
  component: () => import(
    /* webpackChunkName: "route.dashboardIndex" */ './DashboardIndex'
  ),
  children: [
    CustomersActions,
    CustomersIndex,
    CustomersView
  ]
}
