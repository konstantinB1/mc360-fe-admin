export default {
  path: 'customers/add',
  name: 'actions.add',
  component: () => import(
    /* webpackChunkName: "route.customers" */ './CustomersModify'
  )
}
