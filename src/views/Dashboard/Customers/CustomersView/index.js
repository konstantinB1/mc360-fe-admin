export default {
  path: 'customers/view/:id(\\d+)',
  name: 'routeNames.customers_view',
  component: () => import(
    /* webpackChunkName: "route.customers" */ './CustomersView'
  )
}
