export default {
  path: 'customers',
  name: 'routeNames.customers',
  component: () => import(
    /* webpackChunkName: "route.customers" */ './CustomersIndex'
  )
}
