import { Login } from 'components/layout'
import { mapActions } from 'vuex'
import {
  AUTH_NAMESPACE,
  AUTH_WITH_DATA
} from 'store/modules/auth/types'

export default {
  render (h) {
    return h(Login, {
      on: {
        submit: this.loginWithData
      }
    })
  },

  methods: {
    ...mapActions(AUTH_NAMESPACE, {
      loginWithData: AUTH_WITH_DATA
    })
  }
}
