import Login from './Login'
export const LOGIN_PATH = '/admin/login'
export const LOGIN_NAME = 'login'

export default {
  path: LOGIN_PATH,
  name: LOGIN_NAME,
  component: Login
}
