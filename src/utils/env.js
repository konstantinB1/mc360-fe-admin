import UrlPath from './UrlPath'

const {
  NODE_ENV,
  VUE_APP_PROXY_WWW,
  VUE_APP_BASE_PATH
} = process.env

const API_PATH = 'api'
const IS_DEV = NODE_ENV === 'development'

/**
 * We rely on webpack proxy middleware package
 * to proxy all requests to '/api' in dev
 */
const apiRoot = IS_DEV
  ? new UrlPath(VUE_APP_PROXY_WWW)
    .append(VUE_APP_BASE_PATH)
    .append('api')
    .toString(false, true)
  : new UrlPath(UrlPath.browserBase)
    .append(API_PATH)
    .toString(true)

/**
 * Resolve asset paths for images, fonts etc
 *
 * @param {String} path
 */
function assetPath (path) {
  const normalize = new UrlPath(path).toString()

  return IS_DEV
    ? new UrlPath(VUE_APP_PROXY_WWW)
      .append(VUE_APP_BASE_PATH)
      .append(normalize)
      .toString(false, true)
    : path
}

export {
  API_PATH,
  IS_DEV,
  VUE_APP_PROXY_WWW,
  apiRoot,
  assetPath
}
