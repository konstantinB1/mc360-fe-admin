export default function findComponentByName (rootChildren, name) {
  return traverse(rootChildren, name)
}

function traverse (rootChildren, name, foundNodes = []) {
  for (let i = 0; i < rootChildren.length; i++) {
    const childNode = rootChildren[i]

    if (childNode) {
      const nodeName = childNode.$vnode.tag.split('-').pop()

      if (nodeName && nodeName === name) {
        foundNodes.push(childNode)
        continue
      }

      const nextChildren = childNode.$children

      if (nextChildren) {
        traverse(nextChildren, name, foundNodes)
      }
    }
  }

  return foundNodes
}
