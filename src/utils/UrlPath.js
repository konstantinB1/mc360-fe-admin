import { isUndefined, isString, identity } from 'lodash'
const PATH_DELIMITER = '/'
const { isArray } = Array

/**
 * Normalize path array with few config tweaks
 *
 * @param {Array|String} path
 * @param {Object} { delimiter, removeFalsy }
 */
function norm (path, {
  delimiter,
  removeFalsy
}) {
  const arrPath = isArray(path)
    ? path
    : isString(path)
      ? path.split(delimiter)
      : undefined

  if (isUndefined(arrPath)) {
    throw Error('UrlPath Error: Path must be either string or array')
  }

  return removeFalsy
    ? arrPath.filter(identity)
    : arrPath
}

export default class UrlPath {
  /**
   * Creates new UrlPath chainable instance
   * @param {String|Array} path
   * @param {Object} { delimiter, removeFalsy}
   */
  constructor (path, {
    delimiter,
    removeFalsy
  } = {
    delimiter: PATH_DELIMITER,
    removeFalsy: true
  }) {
    this.delimiter = delimiter
    this.path = path
      ? norm(path, {
        delimiter,
        removeFalsy
      })
      : [UrlPath.browserBase]
  }

  /**
   * Get window location path base
   *
   * @return {String}
   */
  static get browserBase () {
    return window.location.pathname.split(PATH_DELIMITER)[1]
  }

  /**
   * Compares paths
   *
   * @param  {String|Array} p1
   * @param  {String|Array} p2
   * @return {Boolean}
   */
  static same (p1, p2) {
    return norm(p1).toString() === norm(p2).toString()
  }

  /**
   * Check if first char is slash
   */
  get _hasFirstSlash () {
    return this.path?.[0].charAt(0) === this.delimiter
  }

  /**
   * Strip first slash from url
   */
  stripFirstSlash () {
    this.path = this._hasFirstSlash
      ? this.path.slice(1)
      : this.path

    return this
  }

  /**
   * Get base path name
   */
  base () {
    this.path = [this.path[this._hasFirstSlash ? 1 : 0]]
    return this
  }

  /**
   * Return path segment with or without
   * including the segment itself
   *
   * @param {String} part
   */
  from (part, withoutPart = true) {
    const index = this.path.findIndex(p => p === part)
    this.path = index !== -1
      ? this.path.slice(withoutPart && this.path[index + 1]
        ? index + 1
        : index
      )
      : this.path

    return this
  }

  /**
   * Append new path
   *
   * @param {String|Array} name
   */
  append (name) {
    if (isArray(name)) {
      name
        .split(this.delimiter)
        .forEach(p => p && this.path.push(p))
    } else {
      this.path.push(name)
    }

    return this
  }

  /**
   * Prepend new path
   *
   * @param {String} part
   */
  prepend (part) {
    this.path.unshift(part)
    return this
  }

  /**
   * Remove part
   *
   * @param {String} part
   */
  remove (part) {
    const index = this.path.find(p => p === part)
    index && this.path.splice(index, 1)

    return this
  }

  /**
   * Pipe to string
   *
   * @return {String} newPath
   */
  toString (absolute, protocol) {
    let str = this.path.join(this.delimiter)

    if (protocol) {
      const protocol = window.location.protocol
      str = `${protocol}//${str}`
    }

    return absolute && !protocol
      ? this.delimiter + str
      : str
  }
}
