import allCountries from 'data/country'

/**
 * Gets only allowed countries list
 *  [format] is false it returns array with keys ie. ['DK', 'RS' ...]
 * If [format] is true it returns object as key value pairs ie. { DKK: 'Denmark" }
 *
 * @param {Array} allowedList
 * @param {Boolean} format
 * @param {String} key
 * @return {Array|Object}
 */
export const getAllowedCountries = (allowedList = [], format = false, key = 'alpha2') => {
  if (!allowedList.length) {
    return allCountries
  }

  const allowed = allowedList.map(country => {
    const findCountry = allCountries.find(o => o[key] === country)

    if (findCountry) {
      return findCountry
    }
  })

  if (format) {
    return allowed.reduce((acc, cur) => {
      acc[cur[key]] = cur.name
      return acc
    }, {})
  }

  return allowed.reduce((acc, cur) => {
    acc.push(cur[key])
    return acc
  }, [])
}

/**
 * Gets countries by key
 *
 * @param  {Array} countries
 * @param  {String} key
 * @return {String}
 */
export const getCountryNames = (
  countries = [],
  key = 'alpha2',
  returnKey = 'name'
) => {
  const format = typeof countries === 'string'
    ? countries.split(', ')
    : countries

  return format.map(country => {
    const getByKey = allCountries.find(c => c[key] === country)

    if (getByKey) {
      return getByKey[returnKey]
    }
  })
}

/**
 * Same as [getAllowedCountries] only it returns all countries,
 * except the ones in [excludeList]
 *
 * @param {Array} allowedList
 * @param {Boolean} format
 * @param {String} key
 * @return {Array|Object}
 */
export const exludeCountries = (
  excludeList = [],
  format = false,
  key = 'alpha2'
) => {
  if (!excludeList.length) {
    return allCountries
  }

  const getFiltered = allCountries.filter(o =>
    !excludeList.includes(o[key])
  )

  if (format) {
    return getFiltered.reduce((acc, cur) => {
      acc[cur[key]] = cur.name
      return acc
    }, {})
  }

  return getFiltered.reduce((acc, cur) => {
    acc.push(cur[key])
    return acc
  }, [])
}

/**
 * Format countries string for multiple countries
 * If no countries are present it returns [-]
 * If [maxDisplayFullNames] is greater than 0 it formats in alphabetical order like 'Azerbejan, Serbia and 200 other countries'
 * If [maxDisplayFullNames] is zero it returns '230 countries'
 * If the length of [countriesData] is the same as all available countries it returns 'All countries'
 *
 * @param  {Array|String} countriesData
 * @param  {Number} maxDisplayFullNames
 * @return {String}
 */
export const countryStringDisplay = function (
  countriesData,
  maxDisplayFullNames = 2,
  key = 'id'
) {
  if (!countriesData || !countriesData.length) {
    return '-'
  }

  const allCountriesArr = typeof countriesData === 'string'
    ? countriesData.split(', ')
    : countriesData

  if (allCountriesArr.length === allCountries.length) {
    return `${this.$localize('all')} ${allCountriesArr.length} ${this.$localize('countries')}`
  }

  const countryNames = getCountryNames(allCountriesArr, key)

  if (
    maxDisplayFullNames &&
    countryNames.length > maxDisplayFullNames
  ) {
    const showNames = []
    const restLen = countryNames.length - maxDisplayFullNames

    for (let i = 0; i < maxDisplayFullNames; i++) {
      showNames.push(countryNames[i])
    }

    return `${showNames.join(', ')} ${this.$localize('and')} ${restLen} ${this.$localize('other_countries')}`
  }

  if (maxDisplayFullNames === 0) {
    return `${countryNames.length} ${this.$localize('countries')}`
  }

  return countryNames.join(', ')
}
