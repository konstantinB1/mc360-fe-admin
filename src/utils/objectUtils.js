export const isObject = function isObject (o) {
  return Object.prototype.toString.call(o) === '[object Object]'
}

/**
 * Copy properties from one to anther object
 * @param  {Object} o1
 * @param  {Object} o2
 * @return {Object}
 */
export const copyObjectProps = function copyObjectProps (o1, o2) {
  const copy = {}
  for (let k in o1)
    if (k in o2 && (o2[k] !== void 0 || o2[k] !== null))
      copy[k] = o2[k] === null ? '' : o2[k]

  return copy
}

/**
 * Format response for post/put API data
 * @param  {Object} errors
 * @return {Object}
 */
export const errorsAPIFormat = function errorsAPIFormat (errors) {
  const format = {}

  for (let k in errors) {
    const cur = errors[k]

    if (cur && typeof cur === 'object' && Object.keys(cur).length)
      format[k] = Object.values(cur)[0] || null
  }

  return format
}

/**
 * Map key values from o2 to o1 as values
 * @param {Object} o1
 * @param {Object} o2
 * @param {Array} e
 */
export const mapKeysFromObj = function mapKeysFromObj (o1 = {}, o2 = {}, e = []) {
  const m = {}
  for (let k in o1) {
    const v1 = o1[k]
    if (
      e && e.length && e.includes(k) ||
      !(k in o2)
    ) continue
    if (!(k in o2)) continue
    const f = o2[k]
    for (let k2 in f) {
      const v2 = f[k2]
      if (v2 && v1 === k2)
        m[k] = v2
    }
  }

  return Object.assign({}, o1, m)
}

/**
 * Encapsulate recursive base argument, if user calls it by mistake
 * just in case
 */
function _flatTraverse (obj, base = {}) {
  for (let k in obj) {
    const cur = obj[k]

    if (isObject(cur))
      _flatTraverse(cur, base)
    else
      base[k] = cur
  }

  return base
}

export const flatObject = function flatObject (obj) {
  return _flatTraverse(obj, {})
}

export const getObjectKeyByValue = function getObjectKeyByValue (value, obj = {}) {
  const entries = Object.entries(obj).find(([ _k, v ]) => v === value)
  return entries && entries.length >= 1 && entries[0]
}
