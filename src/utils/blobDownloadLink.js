export default function blobDownloadLink (blobs, fileName) {
  const url = window.URL.createObjectURL(new Blob(blobs))
  const link = document.createElement('a')
  link.href = url
  link.setAttribute('download', fileName)
  document.body.appendChild(link)
  link.click()
  link.parentNode.removeChild(link)
}
