import isPlainObject from 'lodash/isPlainObject'

function randomFromArr (arr) {
  return arr[Math.floor(Math.random() * arr.length)]
}

export default function (scheema, options = {}) {
  const count = typeof options === 'number' ? options : (options.count || 100)
  const data = []

  for (let i = 0; i < count; i++) {
    const row = {}

    for (const key in scheema) {
      const value = scheema[key]

      if (Array.isArray(value))
        row[key] = randomFromArr(value)
      else if (value === 'index')
        row[key] = i
      else if (!value && isPlainObject(options) && options.skipNullOrUndefined)
        continue
    }

    data.push(row)
  }

  return data
}
