import mcInfo from 'data/mcInfo'

export default vm => ({
  static: [
    {
      name: vm.$tc('routeNames.booking'),
      path: '/admin/booking'
    },
    {
      name: vm.$tc('routeNames.shipments', 2),
      path: '/admin/shipments'
    },
    {
      name: vm.$tc('routeNames.pickup'),
      path: '/admin/pickup'
    }
  ],
  panel: {
    [vm.$t('your_choices')]: [
      {
        name: vm.$t('dashboard'),
        icon: 'stats',
        path: '/'
      },
      {
        name: vm.$t('shipping'),
        icon: 'location',
        items: [
          {
            name: vm.$t('book_shipment'),
            path: '/shipping/book-shipment'
          },
          {
            name: vm.$tc('draft', 2),
            path: '/shipping/drafts'
          },
          {
            name: vm.$tc('shipment', 2),
            path: '/shipping/pickups'
          },
          {
            name: vm.$tc('pickup', 2),
            path: '/shipping/pickups'
          },
          {
            name: vm.$t('end_of_days'),
            path: '/shipping/end-of-days'
          },
          {
            name: vm.$tc('consolidation', 2),
            path: '/shipping/consolidations'
          }
        ]
      },
      {
        name: vm.$t('inventory'),
        icon: 'paper',
        items: [
          {
            name: vm.$tc('product', 2),
            path: '/inventory/products'
          },
          {
            name: vm.$tc('location', 2),
            path: '/inventory/locations'
          },
          {
            name: vm.$tc('zone', 2),
            path: '/inventory/zones'
          },
          {
            name: vm.$t('containers_boxes'),
            path: '/inventory/containers-boxes'
          }
        ]
      },
      {
        name: vm.$t('order_management'),
        icon: 'checklist',
        items: [
          {
            name: vm.$t('sales_orders'),
            path: '/order-management/sales-orders'
          },
          {
            name: vm.$tc('purchase_orders'),
            path: '/order-management/purchase-orders'
          },
          {
            name: vm.$t('delivery_notes'),
            path: '/order-management/delivery-notes'
          },
          {
            name: vm.$t('complete_order'),
            path: '/order-management/complete-order'
          }
        ]
      },
      {
        name: vm.$tc('customers'),
        icon: 'book',
        items: [
          {
            name: vm.$tc('customer', 2),
            path: '/customers/customers'
          },
          {
            name: vm.$tc('type', 2),
            path: '/customers/types'
          }
        ]
      },
      {
        name: vm.$t('connect'),
        icon: 'connect',
        items: [
          {
            name: vm.$tc('store', 2),
            path: '/connect/stores'
          },
          {
            name: vm.$tc('webhook', 2),
            path: '/connect/webhooks'
          },
          {
            name: vm.$tc('automation', 2),
            path: '/connect/automations'
          }
        ]
      },
      {
        name: vm.$t('settings'),
        icon: 'settings',
        items: [
          {
            name: vm.$t('settings'),
            path: '/settings/departments'
          },
          {
            name: vm.$t('upload_templates'),
            path: '/settings/upload-templates'
          },
          {
            name: vm.$tc('emails'),
            path: '/settings/emails'
          },
          {
            name: vm.$tc('printers'),
            path: '/settings/printers'
          },
          {
            name: vm.$t('locations'),
            path: '/settings/locations'
          },
          {
            name: vm.$tc('team'),
            path: '/settings/team'
          },
          {
            name: vm.$t('system'),
            path: '/settings/system'
          },
          {
            name: vm.$t('owners'),
            path: '/settings/owners'
          }
        ]
      }
    ],
    [vm.$t('contact_us')]: [
      {
        name: mcInfo.phone,
        icon: 'mobile'
      },
      {
        name: mcInfo.email,
        icon: 'letter'
      }
    ],
    [vm.$t('company')]: [
      {
        name: vm.$t('about_mc'),
        icon: 'question',
        path: '/about'
      },
      {
        name: vm.$t('terms_of_use'),
        icon: 'terms',
        path: '/terms-of-use'
      }
    ]
  }
})
