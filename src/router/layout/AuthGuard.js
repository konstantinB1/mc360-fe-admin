import { mapState, mapMutations, mapActions } from 'vuex'
import { AUTH_NAMESPACE, AUTH_WITH_TOKEN } from 'store/modules/auth/types'
import { USER_NAMESPACE, SET_USER_DATA } from 'store/modules/user/types'
import { getToken } from 'services/auth/token'
import { TOKEN_KEY } from 'config'
import { setGlobalToken } from 'services/api'
import { fetchUserProfile } from 'services/api/userProfile'

export default {
  name: 'AuthGuard',

  computed: mapState(AUTH_NAMESPACE, [
    'isInitAuth',
    'isFinishedAuth'
  ]),

  methods: {
    ...mapActions(AUTH_NAMESPACE, {
      loginWithToken: AUTH_WITH_TOKEN
    }),

    ...mapMutations(USER_NAMESPACE, {
      setUserData: SET_USER_DATA
    })
  },

  async created () {
    const token = getToken(TOKEN_KEY)

    if (token) {
      setGlobalToken(token)
      const data = await fetchUserProfile()
      data && this.setUserData(data)
    }
  }
}
