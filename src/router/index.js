import Vue from 'vue'
import VueRouter from 'vue-router'
import qs from 'qs'
import Login from 'views/Login'
import DashboardRoutes from 'views/Dashboard'
import AppIndex from 'views/AppIndex'
import DevTest from 'views/_DEV_TEST_'

Vue.use(VueRouter)

const routes = [
  Login,
  {
    path: '/admin',
    component: () => import(/* webpackChunkName: "route.layout" */ './layout/Layout'),
    children: [
      AppIndex,
      DashboardRoutes
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  parseQuery (query) {
    return qs.parse(query, {
      plainObjects: true,
      ignoreQueryPrefix: true
    })
  },
  routes
})

export default router
