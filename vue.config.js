const { resolve } = require('path')
const {
  VUE_APP_BASE_PATH,
  VUE_APP_PROXY_WWW
} = require('dotenv').config().parsed

function path (p = '') {
  return resolve(__dirname, './src', p)
}

module.exports = {
  lintOnSave: false,
  runtimeCompiler: true,
  configureWebpack: {
    devServer: {
      proxy: {
        '/api': {
          target: `${VUE_APP_PROXY_WWW}/${VUE_APP_BASE_PATH}`,
          secure: false,
          changeOrigin: true
        }
      }
    },
    resolve: {
      alias: {
        assets: path('assets'),
        components: path('components'),
        config: path('config'),
        data: path('data'),
        directives: path('directives'),
        i18n: path('i18n'),
        router: path('router'),
        services: path('services'),
        store: path('store'),
        utils: path('utils'),
        views: path('views')
      },
      extensions: ['.js', '.vue', '.json', '.scss']
    }
  }
}
